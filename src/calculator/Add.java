/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author Adeel Khilji
 */
public class Add extends Calculator
{
    protected Add(double operand1, double operand2)
    {
        super(operand1, operand2);
    }
    public double calculate()
    {
        return operand1 + operand2;
    }
}
