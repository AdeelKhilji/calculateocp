/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author Adeel Khilji
 */
public class CalculateDemo 
{
    public static void main(String[] args)
    {
        Add add = new Add(2,2);
        System.out.println("SUM: " + add.calculate());
        Subtract subtract = new Subtract(5,2);
        System.out.println("SUBTRACTION: " + subtract.calculate());
    }
}
